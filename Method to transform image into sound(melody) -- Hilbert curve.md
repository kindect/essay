*Author: TianKai Ma, educator at khan academy, Written 2021/2/13,Sent 2021/2/17*

# Method to transform image into sound(melody) -- Hilbert curve

## Abstract

Use mathematical way to determine the best way to transfer image into sound. Hilbert curve is used and there isn't much math or musical theory the readers need to understand. We'll look at the method and the effect with an parallel ,double-blind experiment, which looks pretty good in result. 

## Pre-Knowledge: Hilbert curve

The **Hilbert curve** (also known as the **Hilbert space-filling curve**) is a continuous fractal space-filling curve  first described by the German mathematician David Hilbert in 1891,as a variant of the space-filling Peano curves discovered by Giuseppe Peano in 1890.

Because it is space-filling, its Hausdorff dimension is 2 (precisely, its image is the unit square, whose dimension is 2 in any definition of dimension; its graph is a compact set homeomorphic to the closed unit interval, with Hausdorff dimension 2).

The Hilbert curve is constructed as a limit of piecewise linear curves. The length of the $n$ th curve is $2^n-\frac {1} {2^n}$, i.e.. , the length grows exponentially with $n$, even though each curve is contained in a square with area $1$

## Pre-Knowledge: Basic music theory

To read this article, basic music theory is required, though not too much.

To start with, think about a piano:

The anatomy of a keyboard (piano/instrument) is pretty simple. It consists of multiple octaves. An *octave* is a set of **12 keys — 7 White ones and 5 Black ones**.

Each key produces a sound at a distinct frequency on pressing, and we press multiple keys simultaneously to play a chord. That’s it!



White Keys are named as **C, D, E, F, G, A, B** whereas Black Keys are named using two terms — **Flat (**b**) and Sharp (**#**)**.

If a black key is on the right side of any white key, it’s called the sharp of that corresponding white key. And, if it is on the left side, it’s called the flat of that corresponding white key.

For example, the black key between C and D has two names: **C#** (C-Sharp) and **Db** (D-Flat). This is because it is on the right side of C and also on the left side of D.

<img src="Method to transform image into sound(melody) -- Hilbert curve.assets/1_MA31WtMa07JEDOgjcI8mvw.png" alt="Image for post" style="zoom: 33%;" />

We’ll use the “Sharp” terminology in this article to avoid confusion. Also, the words, “Notes” and “Keys” are used interchangeably.

## 1. Way to make the Hilbert curve using Python function

There are a few passage on this topic, but most of them are written by mathematicians and discuss about the "real" Hilbert curve(which, in our case they call it a fake one cause the real one is to take $n\to \infin$)

Here are some technical discussion on how to make Hilbert curve 

A curve (so called space filling curve) usually contains the following segments (or parts).

<img src="Method to transform image into sound(melody) -- Hilbert curve.assets/image-20210210135119172.png" alt="image-20210210135119172" style="zoom: 50%;" />

or just to simplify this, use only 7,8,9,10, which is rebuild into this to express

![image-20210212150715427](Method to transform image into sound(melody) -- Hilbert curve.assets/image-20210212150715427.png)

However, this notation can cause several serious problems.

In order to make full use of memory (since computer language start with 0), we sub them with 1

<img src="Method to transform image into sound(melody) -- Hilbert curve.assets/image-20210212165224852.png" alt="	1Q1Q1Q1Q1Q1111QQQQ1" style="zoom: 50%;" />

and there is a 'clever' way of generating the Hilbert curve is to use recursion.

To make a 'n-dim' Hilbert curve, use the n-1 curve and use rotation and flip to generate the new one.

Take a look at the animation here:

> http://bit-player.org/extras/hilbert/hilbert-construction.html

<img src="Method to transform image into sound(melody) -- Hilbert curve.assets/image-20210213195612921.png" alt="image-20210213195612921" style="zoom:67%;" />

To make a $n$ th Hilbert curve, first make the $n-1$ th Hilbert curve. Put it on the top-left and top-right, rotate clockwise on the down-left and counter-clockwise on the down-right.

The complete Python code used here is shown below (License: MIT)

```python
N_MAX=10
definition={'up':0,"right":1,"down":2,"left":3}
dx=[-1,0,1,0]
dy=[0,1,0,-1]
rotate_clk_lambda=lambda x:np.mod((x+1),4)
rotate_cclk_lambda=lambda x:np.mod((x-1),4)
flipud_lambda=lambda x:(np.mod(x,2)==0)*(2-x)+(np.mod(x,2)==1)*x
turtle_definition={'up':90,'right':0,'down':270,'left':180}
angle_change=lambda x:np.mod(5-x,4)*90
HC=[]
HC.append(None)
HC.append(np.array([[1,2],[0,2]]))
for n in range(2,N_MAX):
    tmp=np.empty([2**n]*2,dtype=int)
    tmp[0:2**(n-1),0:2**(n-1)]=HC[n-1]
    tmp[0:2**(n-1),2**(n-1):2**n]=HC[n-1]
    tmp[2**(n-1):2**n,0:2**(n-1)]=flipud_lambda(np.flipud(rotate_clk_lambda(np.rot90(HC[n-1],-1))))
    tmp[2**(n-1):2**n,2**(n-1):2**n]=flipud_lambda(rotate_cclk_lambda(np.flipud(np.rot90(HC[n-1]))))
    tmp[2**(n-1)][0]=0
    tmp[2**(n-1)-1][2**(n-1)-1]=1
    tmp[2**(n-1)-1][2**n-1]=2
    HC.append(tmp)
```

Now we are able to transfer from image into sound.

The advantages of using Hilbert curve function to transform image into sound: that each t in [0,1] indicates a point(x,y) in the 2D space

and for a number in a finite series, we can transform that point into a t, which means the whole image is able to convert to a single variable function.

As 3b1b pointed out, another advantage for this is that when upgrade pixel, for example 256*256 to 512\*512, the Hilbert curve provides a way to make  the upgrade without cost of re-learning and re-adapting the program.

## 2. Take pixels from image and line up.

First to point out that the Hilbert function generate above using that python code, is actually not a "curve". It is a numpy 2D matrix with each box filled with the direction of the next box

We first make sure that the image is stretched to the wanted dimensions, aka 512*512.

This time we use opencv:

```python
input_image=cv2.resize(cv2.imread(input_file),(512,512))
```

now read the pixels in RGB with numpy:

```python
RGBs=np.empty((512*512,3),dtype=np.int32)
x,y=511,0
for i in range(512*512):
    RGBs[i]=np.array(input_image[x][y],dtype=np.int32)
    dir=HC[9][x][y]
    x+=dx[dir]
    y+=dy[dir]
    pass
```

remember the `dx` and `dy` list is defined in the code above

## 3. Make R, G, B into melody of the sound.

When it comes to operate sound in python, as well as making sound, there isn't really too much options.

In this passage, we use mingus, a free open-source python library

Since mingus can only make `midi` file, midi2audio is used to make `.wav`sounds.



The actual hard part of this is to remap R,G,B into one melody. The way to reduce disperse here is by using the `mod` function.

RGB values all range from 0 ~255, and the wanted sound number ranged from 20~65 ($\pm 5$)

The current solution is here:

```python
R=np.mod(RGBs[:,0],15)+20
G=np.mod(RGBs[:,1],15)+35
B=np.mod(RGBs[:,2],15)+50
```

To see the effects, use `pylab` library:

```python
pylab.plot(X,R)
pylab.plot(X,G)
pylab.plot(X,B)
pylab.show()
```

and finally to make a melody, use the `mingus` library(`Note`,`Notecollection`,`Bar`and `Track`classes are quite difficult to operate)

code here:

```python
sound_track=Track()

for i in range(262144):
    if(random.random()<0.0004):
        tmp_notes=[Note().from_int(int(R[i])),Note().from_int(int(G[i])),Note().from_int(int(B[i]))]
        tmp_container=NoteContainer(tmp_notes)
        sound_track.add_notes(tmp_container)
```

The random function is to limit down the amount of pixels, which without limitations would be at least 34 hours LOL.

and finally, nothing to say write it into a wav file:

```python
midi_file_out.write_Track("test.mid",sound_track,bpm=240)
FluidSynth().midi_to_audio('test.mid',output_file)
```

The complete source code here(Again MIT agreement):

```python
import numpy as np
import pylab
import cv2
from PIL import Image
from scipy.io import wavfile
from mingus.containers import NoteContainer,Note,Bar,Track
from mingus.midi import midi_file_out
from midi2audio import FluidSynth
import random
input_file="10.jpg"
output_file="output13.wav"
N_MAX=10
definition={'up':0,"right":1,"down":2,"left":3}
dx=[-1,0,1,0]
dy=[0,1,0,-1]
rotate_clk_lambda=lambda x:np.mod((x+1),4)
rotate_cclk_lambda=lambda x:np.mod((x-1),4)
flipud_lambda=lambda x:(np.mod(x,2)==0)*(2-x)+(np.mod(x,2)==1)*x
turtle_definition={'up':90,'right':0,'down':270,'left':180}
angle_change=lambda x:np.mod(5-x,4)*90
HC=[]
HC.append(None)
HC.append(np.array([[1,2],[0,2]]))
for n in range(2,N_MAX):
    tmp=np.empty([2**n]*2,dtype=int)
    tmp[0:2**(n-1),0:2**(n-1)]=HC[n-1]
    tmp[0:2**(n-1),2**(n-1):2**n]=HC[n-1]
    tmp[2**(n-1):2**n,0:2**(n-1)]=flipud_lambda(np.flipud(rotate_clk_lambda(np.rot90(HC[n-1],-1))))
    tmp[2**(n-1):2**n,2**(n-1):2**n]=flipud_lambda(rotate_cclk_lambda(np.flipud(np.rot90(HC[n-1]))))
    tmp[2**(n-1)][0]=0
    tmp[2**(n-1)-1][2**(n-1)-1]=1
    tmp[2**(n-1)-1][2**n-1]=2
    HC.append(tmp)
CUR_SIZE=512
input_image=cv2.resize(cv2.imread(input_file),(512,512))
X=np.linspace(0,262144,262144,endpoint=False,dtype=np.float32)
RGBs=np.empty((512*512,3),dtype=np.int32)
x,y=511,0
for i in range(512*512):
    RGBs[i]=np.array(input_image[x][y],dtype=np.int32)
    dir=HC[9][x][y]
    x+=dx[dir]
    y+=dy[dir]
R=np.mod(RGBs[:,0],15)+20
G=np.mod(RGBs[:,1],15)+35
B=np.mod(RGBs[:,2],15)+50
sound_track=Track()
for i in range(262144):
    if(random.random()<0.0004):
        tmp_notes=[Note().from_int(int(R[i])),Note().from_int(int(G[i])),Note().from_int(int(B[i]))]
        tmp_container=NoteContainer(tmp_notes)
        sound_track.add_notes(tmp_container)
midi_file_out.write_Track("test.mid",sound_track,bpm=240)
FluidSynth().midi_to_audio('test.mid',output_file)
```

## 4. Test the results:

The experiment cannot be done without the help from the following volunteers. Many thx to them:

> @Mohammad Elayan
> @Darth Bane
> @Ricardo Pestana
> @kulva87
> @RLR
> @Wozami
> @Antrissvox
>
> @wen20040501
> @z*q1144316798

There's also some who I failed to get their names and brought them credit and mostly on a anonymous web pull

There's approximately 183 people present in this experiment, which should have a pm 1 people mistake cause apparently someone leave the web pull halfway.

To see the result, let's first check the "trick question": four sounds generated using one picture:

<img src="Method to transform image into sound(melody) -- Hilbert curve.assets/Figure_1.png" style="zoom: 15%;" /><img src="Method to transform image into sound(melody) -- Hilbert curve.assets/Figure_3.png" style="zoom: 50%;" />

Goes with A: 25.13% Goes with B: 23.49% Goes with C:26.22%,Goes with D: 25.13%

The result is fairly close so there is something to hope.

The first question is a kind of hard, the provided 4 images are like:

<img src="Method to transform image into sound(melody) -- Hilbert curve.assets/4.jpg" style="zoom: 5%;" /><img src="Method to transform image into sound(melody) -- Hilbert curve.assets/5.jpg" style="zoom: 15%;" /><img src="Method to transform image into sound(melody) -- Hilbert curve.assets/6.jpg" style="zoom: 15%;" /><img src="Method to transform image into sound(melody) -- Hilbert curve.assets/7.jpg" style="zoom: 15%;" />

The result showed a slightly high in answer D, but not that significant. Actually the difference is so small and could be neglectable:

 <img src="Method to transform image into sound(melody) -- Hilbert curve.assets/Figure_2.png" style="zoom: 50%;" /><img src="Method to transform image into sound(melody) -- Hilbert curve.assets/Figure_4.png" style="zoom: 33%;" />

The rate for desired D is 27.56% slightly higher than the trick problem. Which I think hardly demonstrates the problem. 

The second question is slightly different. We used two photos to generate 4 sounds, which means 1photo for 3 and the other one for 1, use this as a test to find out whether human can discover the difference between different photos.

Results here:

![](Method to transform image into sound(melody) -- Hilbert curve.assets/Figure_5.png)

> The correct answer is C

As to further demonstrates the problem, we limit down the number to 3, and the result comes better:

![](Method to transform image into sound(melody) -- Hilbert curve.assets/Figure_6.png)

The experiment is done completely parallel, double blind. Each wav file generate for each question is numbered in a random number ranged from 1000 to 9999, and proved this method of transform can make human learn that there's connection between sounds generated from 1 photo.

## 5. Conclusion

There's still a long way to go.

First, to point out is that neither the sound nor the photo have gone through noise-cancelling. This results in problems with the output sound.

Second, the program didn't addressed the jumping non-linear problem. It used a random function to skip some of the pixels. Resulting in a slightly different sound each time generated. Though the result shows that human do can tell the difference, a major concern for this is that human don't actually know the details of the picture.

To work on this project furthermore, we can first apply suitable noise-canceling algorithm, and then delete the background. Re-engine the stretch function so pixels after change don't lose too much information. And more importantly, re-engine the output function so that it don't use the piano sound, which human can only afford 120 bpm, make 2/4 1/4 sounds, not just 4/4 sounds.

Anyway, as I have said, there's still a long way to go. As we continue working on this project, we may find a solution for those visually disabled.